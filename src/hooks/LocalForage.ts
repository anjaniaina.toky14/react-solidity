import { useContext, useState } from 'react';
import { LocalForage } from '../context/LocalForageContext';

export const useGetLocalForageContext = (name: string) => {
  const { forages, setLocalForage } = useContext(LocalForage);
  return { forage: forages[name], setLocalForage };
};

export const useGetLocalForageValue = <Type>(localForageItem: LocalForage, key: string) => {
  const [getlocalForage, setGetlocalForage] = useState<Type | null>(null);

  localForageItem.getItem<Type>(key).then((data) => setGetlocalForage(data));

  return getlocalForage;
};
