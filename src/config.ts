const dotenvConfigs = {
  apiUrl: import.meta.env.VITE_API_URL,
  lensHubContractAddress: import.meta.env.VITE_LENS_HUB_CONTRACT_ADDRESS,
};

export default dotenvConfigs;
