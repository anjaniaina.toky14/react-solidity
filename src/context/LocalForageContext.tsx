import localForage from 'localforage';
import { FC, PropsWithChildren, createContext, useContext } from 'react';

type TlocalForage = { [key: string]: LocalForage };

interface ILocalForage {
  forages: TlocalForage;
  setLocalForage: <Type>(localForageItem: LocalForage, key: string, value: Type) => void;
}

var store = localForage.createInstance({
  name: 'local_storage1',
});

store.config({
  driver: [localForage.LOCALSTORAGE, localForage.INDEXEDDB],
  name: 'local_storage1',
});

const LocalForages: TlocalForage = {
  local_storage1: store,
};

export const LocalForage = createContext<ILocalForage>({
  forages: LocalForages,
  setLocalForage: () => {},
});

const LocalForageContext: FC<PropsWithChildren> = ({ children }) => {
  const handleSetLocalForage = <Type,>(localForageItem: LocalForage, key: string, value: Type) => {
    localForageItem.setItem<Type>(key, value);
  };

  return (
    <LocalForage.Provider
      value={{
        forages: LocalForages,
        setLocalForage: handleSetLocalForage,
      }}
    >
      {children}
    </LocalForage.Provider>
  );
};

export default LocalForageContext;
