import { FC, PropsWithChildren } from 'react';
import LocalForageContext from './LocalForageContext';

// 1. import `ChakraProvider` component
import { ChakraProvider } from '@chakra-ui/react';
import UrqlContext from './Urql';

const Providers: FC<PropsWithChildren> = ({ children }) => {
  return (
    <UrqlContext>
      <ChakraProvider>
        <LocalForageContext>{children}</LocalForageContext>
      </ChakraProvider>
    </UrqlContext>
  );
};

export default Providers;
