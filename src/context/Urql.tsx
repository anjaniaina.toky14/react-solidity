import { FC, PropsWithChildren, createContext, useContext } from 'react';
import { Provider } from 'urql';
import { client } from '../graphql/Urql';

const UrqlContext: FC<PropsWithChildren> = ({ children }) => {
  return <Provider value={client}>{children}</Provider>;
};

export default UrqlContext;
