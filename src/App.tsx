import './App.css';
import Content from './components/Content';
import Navbar from './components/Navbar';
import dotenvConfigs from './config';
import Providers from './context/Providers';

console.log('dotenvConfigs :>> ', dotenvConfigs);

function App() {
  return (
    <Providers>
      <div className="app background-gradient">
        {/* Head */}
        <Navbar />

        {/* Content */}
        <Content />
      </div>
    </Providers>
  );
}

export default App;
