import { gql } from 'urql';
import { CommentFields, MirrorFields, PostFields } from '../Fragments/Publication';

export const QUERY_EXPORE_PUBLICATIONS = gql`
  query ExplorePublications {
    explorePublications(
      request: { sortCriteria: TOP_COMMENTED, publicationTypes: [POST, COMMENT, MIRROR], limit: 10 }
    ) {
      items {
        __typename
        ... on Post {
          ...PostFields
        }
        ... on Comment {
          ...CommentFields
        }
        ... on Mirror {
          ...MirrorFields
        }
      }
      pageInfo {
        prev
        next
        totalCount
      }
    }
  }
  ${MirrorFields}
  ${CommentFields}
  ${PostFields}
`;
