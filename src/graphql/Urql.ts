import { Client, cacheExchange, fetchExchange } from 'urql';
import dotenvConfigs from '../config';

export const client = new Client({
  url: dotenvConfigs.apiUrl,
  exchanges: [cacheExchange, fetchExchange],
});
