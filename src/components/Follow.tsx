import { FC, useState } from 'react';
import { ethers, encodeBytes32String } from 'ethers';
import dotenvConfigs from '../config';
import { Box, IconButton } from '@chakra-ui/react';
import { BsPersonAdd } from 'react-icons/bs';

interface IFollowProps {
  post: any;
}

const Follow: FC<IFollowProps> = ({ post }) => {
  const [followed, setFollowed] = useState([]);

  const follow = async (_post: any) => {
    const id = _post.id;
    let signer = null;

    let provider;
    if (window.ethereum == null) {
      // If MetaMask is not installed, we use the default provider,
      // which is backed by a variety of third-party services (such
      // as INFURA). They do not have private keys installed so are
      // only have read-only access
      console.log('MetaMask not installed; using read-only defaults');
      provider = ethers.getDefaultProvider('ropsten');
    } else {
      // Connect to the MetaMask EIP-1193 object. This is a standard
      // protocol that allows Ethers access to make all read-only
      // requests through MetaMask.
      provider = new ethers.BrowserProvider(window.ethereum);

      // It also provides an opportunity to request access to write
      // operations, which will be performed by the private key
      // that MetaMask manages for the user.
      signer = await provider.getSigner();
    }

    const contract = new ethers.Contract(dotenvConfigs.lensHubContractAddress, 'ABI', signer);

    // @ts: dis
    const ids = [parseInt(id)];
    const bytes = [encodeBytes32String('0x0')];

    console.log('_post :>> ', _post);
    console.log('id :>> ', id);
    console.log('ids :>> ', ids);
    console.log('bytes :>> ', bytes);

    const tx = await contract.follow(ids, bytes);
    console.log('tx :>> ', tx);
    await tx.wait();
  };

  return (
    <Box height="50px" _hover={{ cursor: 'pointer' }}>
      <IconButton
        colorScheme="none"
        aria-label="Search database"
        icon={<BsPersonAdd />}
        onClick={follow.bind(null, post)}
      />
    </Box>
  );
};

export default Follow;
