import { Box } from '@chakra-ui/react';
import { FC } from 'react';
import Signin from './Signin';

const Navbar: FC = () => {
  return (
    <Box width="100%" backgroundColor="rgba(5, 32, 64, 28)">
      <Box
        display="flex"
        justifyContent="space-between"
        alignItems="center"
        width="55%"
        margin="auto"
        color="white"
        padding="10px 0"
      >
        <Box>
          <Box fontFamily="DMSerifDisplay" fontSize="44px" fontStyle="italic">
            DECENTRA
          </Box>
          <Box>Decentralized Social Media App</Box>
        </Box>
        <Signin />
      </Box>
    </Box>
  );
};

export default Navbar;
