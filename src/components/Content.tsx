import { Box } from '@chakra-ui/react';
import { FC } from 'react';

const Content: FC = () => {
  return (
    <Box
      display="flex"
      justifyContent="space-between"
      width="55%"
      margin="35px auto auto auto"
      color="white"
    >
      Content
    </Box>
  );
};

export default Content;
