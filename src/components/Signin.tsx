import { Box, Button } from '@chakra-ui/react';
import { FC, useState } from 'react';

const Signin: FC = () => {
  const [account, setAccount] = useState(null);

  const signIn = async () => {
    if (window.ethereum) {
      try {
        window.ethereum.request<any>({ method: 'eth_requestAccounts' }).then((acc) => {
          console.log('acc :>>', acc);

          if (acc) {
            setAccount(acc[0]);
          }
        });
      } catch (err) {
        console.log('user did not add account...', err);
      }
    }
  };

  if (account) {
    return (
      <Box backgroundColor="#000" padding="15px" borderRadius="6px">
        Connected
      </Box>
    );
  }

  return (
    <Box>
      <Button
        onClick={signIn}
        color="rgba(5, 32, 64)"
        _hover={{
          backgroundColor: '#808080',
        }}
      >
        Connect
      </Button>
    </Box>
  );
};

export default Signin;
