import { Box, Icon, Image } from '@chakra-ui/react';
import { FC, useState } from 'react';
import { BsPersonCircle } from 'react-icons/bs';

interface IImageProps {
  url?: string;
}

const ImageContainer: FC<IImageProps> = ({ url }) => {
  const [imageError, setImageError] = useState(false);

  const parseImgUrl = (_url?: string) => {
    if (_url && _url.startsWith('ipfs:')) {
      const ipfsHash = _url.split('//')[1];
      return `https://ipfs.io/ipfs/${ipfsHash}`;
    }
    return url;
  };

  return (
    <Box w="75px" h="75px" mt="8px">
      {!imageError ? (
        <Image
          width="75px"
          height="75px"
          alt="profile"
          src={parseImgUrl(url)}
          onError={({ currentTarget }) => {
            currentTarget.onerror = null;
            setImageError(true);
          }}
        />
      ) : (
        <Icon as={BsPersonCircle} w="50px" h="50px" />
      )}
    </Box>
  );
};

export default ImageContainer;
